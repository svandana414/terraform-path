resource "aws_subnet" "public_subnet" {
  cidr_block              = var.public_cidrs
  vpc_id                  = var.myvpc
  availability_zone      = "us-east-2a"
tags = {
    Name = "public-subnet1"
  }
}
