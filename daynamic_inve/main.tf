provider "aws" {
  region = "us-east-2"
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source          = "./vpc"
  vpc_cidr        = "192.168.0.0/24"
  public_cidrs    = "192.168.0.0/28"
  private_cidrs    = ["192.168.0.16/28", "192.168.0.32/28"]
  myvpc             = module.vpc.vpc_id
  
}
 
