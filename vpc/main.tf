
provider "aws" {
  region = "us-east-2"
}

data "aws_availability_zones" "available" {}


resource "aws_vpc" "myvpc" {
  cidr_block  = var.vpc_cidr
  tags = {
    Name = "terraform_vpc"
  }
}

resource "aws_subnet" "public_subnet" {
  cidr_block              = var.public_cidrs
  vpc_id                  = aws_vpc.myvpc.id
  map_public_ip_on_launch = true
  availability_zone      = "us-east-2a"
tags = {
    Name = "public-subnet1"
  }
}

resource "aws_subnet" "private_subnet" {
  count                  = 2
  cidr_block              = var.private_cidrs[count.index]
  vpc_id                  = aws_vpc.myvpc.id
  availability_zone      = data.aws_availability_zones.available.names[count.index]
tags = {
    Name = "private-subnet.${count.index + 1}"
  }
}

resource "aws_internet_gateway"  "igw" {
 vpc_id          = aws_vpc.myvpc.id
 tags = {
      Name = "igw"
  }
} 

resource "aws_route_table"  "public_RT" {
 vpc_id          = aws_vpc.myvpc.id
route {
  cidr_block    = "0.0.0.0/0"
  gateway_id    = aws_internet_gateway.igw.id
}

 tags = {
      Name = "public_RT"
  }
 }


resource "aws_route_table"  "private_RT" {
vpc_id         = aws_vpc.myvpc.id
route {
  nat_gateway_id = aws_nat_gateway.nat.id
  cidr_block    = "0.0.0.0/0"
 }
 tags = {
      Name = "private_RT"
  }
 }


resource "aws_eip" "eip-1" {
 vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip-1.id
  subnet_id     = aws_subnet.public_subnet.id
   tags = {
    Name = "nat"
  }
}


resource "aws_route_table_association" "rt1_association"{
    subnet_id = aws_subnet.public_subnet.id
    route_table_id = aws_route_table.public_RT.id
}

resource "aws_route_table_association" "rt2_association"{
    subnet_id = aws_subnet.private_subnet[0].id
    route_table_id = aws_route_table.private_RT.id
}


resource "aws_route_table_association" "rt3_association"{
    subnet_id = aws_subnet.private_subnet[1].id
    route_table_id = aws_route_table.private_RT.id
}


resource "aws_security_group" "SecurityGroup" {
  name        = "SG"
  description = "Project inbound traffic"
  vpc_id      = aws_vpc.myvpc.id

  ingress {
    description = "sg1"
    from_port   = var.fromPort
    to_port     = var.toPort
    protocol    = var.protocol1
    cidr_blocks = var.cidr1
  }
  
 egress {
    description = "sg2"
    from_port   = var.from-Port
    to_port     = var.to-Port
    protocol    = var.protocol2
    cidr_blocks = var.cidr2
  }

  tags = {
    Name = "projectSG"
  }
}


resource "aws_instance" "my-instance1" {
  ami             = "ami-0f7919c33c90f5b58"
  associate_public_ip_address = "true"
  instance_type   = "t2.micro"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.public_subnet.id
 tags = {
  Name = "jump_jfrog1"
  }
}

resource "aws_instance" "my-instance2" {
  ami             = "ami-0f7919c33c90f5b58"
  instance_type   = "t2.micro"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.private_subnet[0].id 
 tags = {
  Name = "instance_jfrog2"
  }
}


resource "aws_instance" "my-instance3" {
  ami             = "ami-0f7919c33c90f5b58"
  instance_type   = "t2.micro"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.private_subnet[1].id
 tags = {
  Name = "instance_jfrog3"
  }
}

