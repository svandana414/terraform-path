data "aws_availability_zones" "available" {}

resource "aws_vpc" "myvpc" {
cidr_block = var.vpc_cidr
tags = {
Name = "terraform_vpc"
  }
}

resource "aws_subnet" "mysubnet-public" {
count = 2
cidr_block = var.subnet1_cidr[count.index]
vpc_id = var.vpc_id
map_public_ip_on_launch = true
availability_zone =  data.aws_availability_zones.available.names[count.index]
tags = {
Name = "public-subnet.${count.index+1}"
  }
}

resource "aws_subnet" "mysubnet-private" {
count = 2
cidr_block = var.subnet2_cidr[count.index]
vpc_id = var.vpc_id
availability_zone =  data.aws_availability_zones.available.names[count.index]
tags = {
Name = "private-subnet.${count.index+1}"
  }
}

resource "aws_internet_gateway" "igw" {
vpc_id = var.vpc_id
tags = {
Name = "igw"
 }
}

resource "aws_route_table" "public-RT" {
vpc_id = var.vpc_id
route {
cidr_block = "0.0.0.0/0"
gateway_id = aws_internet_gateway.igw.id
}
tags = {
Name = "public_RT"
 }
}

resource "aws_route_table" "private-RT" {
vpc_id = var.vpc_id
route {
cidr_block = "0.0.0.0/0"
nat_gateway_id = aws_nat_gateway.nat.id
}
tags = {
Name = "private_RT"
 }
}

resource "aws_eip" "eip" {
vpc = true
}

resource "aws_nat_gateway" "nat" {
allocation_id = aws_eip.eip.id
subnet_id = aws_subnet.mysubnet-public[0].id 
tags = {
Name = "mygateway"
 }
}


resource "aws_route_table_association" "rt1_association" {
subnet_id = aws_subnet.mysubnet-public[0].id
route_table_id = aws_route_table.public-RT.id
}

resource "aws_route_table_association" "rt2_association" {
subnet_id = aws_subnet.mysubnet-public[1].id
route_table_id = aws_route_table.public-RT.id
}

resource "aws_route_table_association" "rt3_association" {
subnet_id = aws_subnet.mysubnet-private[0].id
route_table_id = aws_route_table.private-RT.id
}

resource "aws_route_table_association" "rt4_association" {
subnet_id = aws_subnet.mysubnet-private[1].id
route_table_id = aws_route_table.private-RT.id
}

