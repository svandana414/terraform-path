data "aws_availability_zones" "available" {
  state = "available"
}


#data "aws_ebs_volume" "snap-volume" {
#most_recent = true
#filter {
#name = "volume-type"
#values = ["gp2"]
#}

#filter {
#    name = "tag:Name"
#   values = ["production"]
#}
#}


resource "aws_ebs_volume" "production-volume" {
  count             = 1
  availability_zone = data.aws_availability_zones.available.names[count.index]
  size              = 100
  tags = {
    Name = "production"
  }
}


resource "aws_ebs_snapshot" "production-snapshot" {
  volume_id = aws_ebs_volume.production-volume[0].id
  tags = {
    Name = "product"
  }
}


resource "aws_key_pair" "mytest-key" {
  public_key = "${file(var.my_public_key)}"
}


resource "aws_instance" "my_instance" {
  count         = 2
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = aws_key_pair.mytest-key.id
  subnet_id     = var.subnet_id[count.index]
  tags = {
    Name = "instance.${count.index + 1}"
  }
}

resource "aws_volume_attachment" "volume_attach" {
  device_name = "/dev/xvdf"
  volume_id   = aws_ebs_volume.production-volume[0].id
  instance_id = aws_instance.my_instance[0].id
}




