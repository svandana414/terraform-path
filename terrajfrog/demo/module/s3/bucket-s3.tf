provider "aws" {
  region = "us-east-2"
}

terraform {
 backend "s3" {
  bucket = "terraform-storage-s3"
  key = "global/s3/terraform.tfstate"
  region = "us-east-2"
  dynamodb_table = "terraform-up-and-running-locks"
  encrypt      = true
 }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

