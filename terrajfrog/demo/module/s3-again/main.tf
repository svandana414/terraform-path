
# terraform {
# backend "s3" {
 # bucket         = "terraform-storage-vandana"
 #key            = "global/s3/terraform.tfstate"
 #region         = "us-east-2"
 #dynamodb_table = "terraform-locking"
 #encrypt        = true
 #}
#}

resource "aws_s3_bucket" "my-bucket" {
bucket = var.terraform-storage-vandana

    versioning {
      enabled = true
    }

    lifecycle {
      prevent_destroy = false
    }


server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
  }
 }
}

 #resource "aws_dynamodb_table" "terraform_locks" {
 # name         = "terraform-locking"
 # billing_mode = "PAY_PER_REQUEST"
 # hash_key     = "LockID"
  #attribute {
  #  name = "LockID"
   # type = "S"
  #}
 # }

