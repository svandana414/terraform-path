resource "aws_security_group" "securitygroup" {
name = var.security-group
description = "security-project"
vpc_id = var.vpc_id

 ingress {
    description = "sg1"
    from_port   = var.fromPort
    to_port     = var.toPort
    protocol    = var.protocol1
    cidr_blocks = var.cidr1
  }

 egress {
    description = "sg2"
    from_port   = var.from-Port
    to_port     = var.egPort
    protocol    = var.protocol2
    cidr_blocks = var.cidr2
  }

  tags = {
    Name = "projectSG"
  }
}

