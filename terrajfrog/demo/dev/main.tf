provider "aws" {
  region = "us-east-2"
}

module "my_vpc" {
  source       = "../module/vpc"
  vpc_cidr     = "192.168.0.0/16"
  vpc_id       = module.my_vpc.vpc_id
  subnet1_cidr = ["192.168.1.0/24", "192.168.2.0/24"]
  subnet2_cidr = ["192.168.3.0/24", "192.168.4.0/24"]
}


module "my_ec2" {
  source        = "../module/ec2"
  ami_id        = "ami-0f7919c33c90f5b58"
  instance_type = "t2.micro"
  subnet_id     = module.my_vpc.subnet_id
}

module "my_s3" {
  source       = "../module/s3"
  my-bucket-s3 = "s3-bucket-terra"
}





















