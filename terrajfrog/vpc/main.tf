provider "aws" {
  region = "us-east-2"
  access_key = "AKIAQCT7FHHQGPG3VNGX"
  secret_key = "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"
}

data "aws_availability_zones" "available" {}


resource "aws_vpc" "myvpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags                 = {
  Name                 = "terraform_vpc"
  }
}

resource "aws_subnet" "public_subnet" {
  count                   = 2
  cidr_block              = var.public_cidrs[count.index]
  vpc_id                  = aws_vpc.myvpc.id
  map_public_ip_on_launch = true
  availability_zone      =  data.aws_availability_zones.available.names[count.index]
tags = {
    Name = "public-subnet.${count.index + 1}"
  }
}

resource "aws_subnet" "private_subnet" {
  count                  = 2
  cidr_block              = var.private_cidrs[count.index]
  vpc_id                  = aws_vpc.myvpc.id
  availability_zone      = data.aws_availability_zones.available.names[count.index]
tags = {
    Name = "private-subnet.${count.index + 1}"
  }
}

resource "aws_internet_gateway"  "igw" {
 vpc_id          = aws_vpc.myvpc.id
 tags = {
      Name = "igw"
  }
} 

resource "aws_route_table"  "public_RT" {
 vpc_id          = aws_vpc.myvpc.id
route {
  cidr_block    = "0.0.0.0/0"
  gateway_id    = aws_internet_gateway.igw.id
}

 tags = {
      Name = "public_RT"
  }
 }


resource "aws_route_table"  "private_RT" {
vpc_id         = aws_vpc.myvpc.id
route {
  nat_gateway_id = aws_nat_gateway.nat.id
  cidr_block    = "0.0.0.0/0"
 }
 tags = {
      Name = "private_RT"
  }
 }


resource "aws_eip" "eip-1" {
 vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip-1.id
  subnet_id     = aws_subnet.public_subnet[0].id
   tags = {
    Name = "nat"
  }
}


resource "aws_route_table_association" "rt1_association"{
    subnet_id = aws_subnet.public_subnet[0].id
    route_table_id = aws_route_table.public_RT.id
}

resource "aws_route_table_association" "rt2_association"{
    subnet_id = aws_subnet.public_subnet[1].id
    route_table_id = aws_route_table.public_RT.id
}


resource "aws_route_table_association" "rt3_association"{
    subnet_id = aws_subnet.private_subnet[0].id
    route_table_id = aws_route_table.private_RT.id
}


resource "aws_route_table_association" "rt4_association"{
    subnet_id = aws_subnet.private_subnet[1].id
    route_table_id = aws_route_table.private_RT.id
}


resource "aws_security_group" "SecurityGroup" {
  name        = "SG"
  description = "Project inbound traffic"
  vpc_id      = aws_vpc.myvpc.id

  ingress {
    description = "sg1"
    from_port   = var.fromPort
    to_port     = var.toPort
    protocol    = var.protocol1
    cidr_blocks = var.cidr1
  }
  
 egress {
    description = "sg2"
    from_port   = var.from-Port
    to_port     = var.to-Port
    protocol    = var.protocol2
    cidr_blocks = var.cidr2
  }

  tags = {
    Name = "projectSG"
  }
}


resource "aws_instance" "my-instance1" {
  ami             = "ami-0f7919c33c90f5b58"
  associate_public_ip_address = "true"
  instance_type   = "t2.xlarge"
  /*
  key_name       = "ohio-key"
  */
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.public_subnet[0].id
  /*
  user_data = <<-EOF
            #!/bin/bash 
           sudo yum update –y
           sudo yum install git -y
           sudo wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u141-b15/336fa29ff2bb4ef291e347e091f7f4a7/jdk-8u141-linux-x64.rpm
           sudo rpm -ivh jdk-8u141-linux-x64.rpm
           
           sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
           sudo rpm --import http://pkg.jenkins.io/redhat/jenkins.io.key
           sudo yum install jenkins -y
           sudo  systemctl start jenkins.service
           sudo chkconfig jenkins on
           sudo cat /var/lib/jenkins/secrets/initialAdminPassword
	   echo "##################Printing JAVA_HOME ###########################################"
          
	   echo 'export JAVA_HOME=/usr/java/jdk1.8.0_141/' >> /etc/profile.d/java_path.sh
           echo 'export PATH=$PATH:$JAVA_HOME/bin'  >> /etc/profile.d/java_path.sh
           pwd
           echo "###########################################user#################################################################"
           whoami
	   cat /etc/profile.d/java_path.sh
           chmod 777 /etc/profile.d/java_path.sh
           source /etc/profile.d/java_path.sh
           echo $JAVA_HOME
           EOF
   */
    
 tags = {
  Name = "jump_jfrog1"
  }
}

resource "aws_instance" "my-instance2" {
  ami             = "ami-0f7919c33c90f5b58"
  associate_public_ip_address = "true"
  instance_type   = "t2.xlarge"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.public_subnet[1].id
 user_data = <<-EOF
            #!/bin/bash
           sudo yum update –y
           sudo yum install git -y
           sudo wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u141-b15/336fa29ff2bb4ef291e347e091f7f4a7/jdk-8u141-linux-x64.rpm
           sudo rpm -ivh jdk-8u141-linux-x64.rpm

           sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
           sudo rpm --import http://pkg.jenkins.io/redhat/jenkins.io.key
           sudo yum install jenkins -y
           sudo  systemctl start jenkins.service
           sudo chkconfig jenkins on
           sudo cat /var/lib/jenkins/secrets/initialAdminPassword
           echo "##################Printing JAVA_HOME ###########################################"

           echo 'export JAVA_HOME=/usr/java/jdk1.8.0_141/' >> /etc/profile.d/java_path.sh
           echo 'export PATH=$PATH:$JAVA_HOME/bin'  >> /etc/profile.d/java_path.sh
           pwd
           echo "###########################################user#################################################################"
           whoami
           cat /etc/profile.d/java_path.sh
           chmod 777 /etc/profile.d/java_path.sh
           source /etc/profile.d/java_path.sh
           echo $JAVA_HOME
           EOF
 tags = {
  Name = "jump_jfrog2"
  }
}


resource "aws_instance" "my-instance3" {
  ami             = "ami-0f7919c33c90f5b58"
  instance_type   = "t2.xlarge"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.private_subnet[0].id 
  user_data = <<-EOF
              #!/bin/bash
              sudo yum install git -y
              cd /home;sudo git clone https://gitlab.com/svandana414/my-personal.git
              sudo yum update -y
              sudo amazon-linux-extras install ansible2
              cd /home/my-personal;sudo ansible-playbook artifect.yml
              EOF
 tags = {
  Name = "private_jfrog1"
  }
}


resource "aws_instance" "my-instance4" {
  ami             = "ami-0f7919c33c90f5b58"
  instance_type   = "t2.xlarge"
  key_name       = "ohio-key"
  security_groups = [aws_security_group.SecurityGroup.id]
  subnet_id    = aws_subnet.private_subnet[1].id
 user_data = <<-EOF
              #!/bin/bash
              sudo yum install git -y
              cd /home;sudo git clone https://gitlab.com/svandana414/my-personal.git
              sudo yum update -y
              sudo amazon-linux-extras install ansible2
              cd /home/my-personal;sudo ansible-playbook artifect.yml
              EOF
 tags = {
  Name = "private_jfrog2"
  }
}


#############################load balancer#######################

resource "aws_lb_target_group" "target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "test-tg1"
  port        = 8082
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.myvpc.id
}

resource "aws_lb" "test"{
 name       = "application-lb"
 internal   = false
 load_balancer_type = "application"
 security_groups    = [aws_security_group.SecurityGroup.id]
 subnets            =  aws_subnet.public_subnet.*.id
 ip_address_type    = "ipv4"
 tags = {
    Name = "test-alb"
  }
}

resource "aws_lb_listener" "test-rule1"{
load_balancer_arn  = aws_lb.test.arn	
port              = 8082
protocol          = "HTTP"
default_action {
type        = "forward"
target_group_arn = aws_lb_target_group.target-group.arn
 }
}

resource "aws_lb_target_group_attachment" "target-group-attachment1" {
target_group_arn  = aws_lb_target_group.target-group.arn
target_id        =  aws_instance.my-instance3.id
port            =   8082
}

resource "aws_lb_target_group_attachment" "target-group-attachment2" {
target_group_arn  = aws_lb_target_group.target-group.arn
target_id        =  aws_instance.my-instance4.id
port            =   8082
}


